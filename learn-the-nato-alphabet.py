"""Tiny script for learning the NATO alphabet."""
from __future__ import print_function
import random
import sys

COLORS = {
    "clear": "\033[0m",
    "bold": "\033[1m",
    "black": "\033[30m",
    "red": "\033[31m",
    "green": "\033[32m",
    "yellow": "\033[33m",
    "blue": "\033[34m",
    "magenta": "\033[35m",
    "cyan": "\033[36m",
    "white": "\033[37m",
}

ALPHABET = [
    "alfa",
    "bravo",
    "charlie",
    "delta",
    "echo",
    "foxtrot",
    "golf",
    "hotel",
    "india",
    "juliet",
    "kilo",
    "lima",
    "mike",
    "november",
    "oscar",
    "papa",
    "quebec",
    "romeo",
    "sierra",
    "tango",
    "uniform",
    "victor",
    "whiskey",
    "x-ray",
    "yankee",
    "zulu",
]


def test():
    """Run our NATO alphabet test."""
    history = []
    while True:
        wrong = 0
        random.shuffle(ALPHABET)
        for item in ALPHABET:
            print("What word replaces {cyan}{}{clear}?".format(item[0], **COLORS))
            answer = input("{yellow}>{clear} ".format(**COLORS)).strip()
            if answer.lower() == item:
                print("{green}Correct!{clear}".format(**COLORS))
                continue
            wrong += 1
            print("{magenta}The correct answer is{clear}: {}".format(item, **COLORS))
        print(
            "You completed the alphabet with {n} wrong.".format(
                n=wrong,
            ),
            end=" ",
        )
        history.append(wrong)
        print("Go again? [Y|n] ", end="")
        go_again = sys.stdin.readline().strip()
        if go_again.lower() in {"no", "n"}:
            break
        print("{blue}Shuffling ...{clear}".format(**COLORS))
    print("\nStatistics")
    print("==========")
    for i, number_wrong in enumerate(history, start=1):
        print(
            "{i:>2}: {blue}{correct}{clear}/{yellow}26{clear}".format(
                i=i, correct=(26 - number_wrong), **COLORS
            )
        )


try:
    test()
except KeyboardInterrupt:
    print("")
    print("")
finally:
    print("{green}Good job today!{clear}".format(**COLORS))
